# autoclick

run this then hold down a button to auto click real fast

usage:
```bash
ruby path/to/autoclick.rb --time-between=0.05 --mouse=8 --trigger='button[10]=down'
```
