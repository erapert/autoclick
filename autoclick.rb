#!/usr/bin/env ruby

require 'optparse'

# mouse is the mouse id from `xinput --list`
# trigger is the string to look for in `xev` output. Hold this button down to autoclick, release to stop
$cfg = Struct.new(:verbose, :between, :mouse_id, :trigger).new(false, 0.1, 8, 'button 10')

OptionParser.new do |opts|
	opts.banner = <<~BANNER
		Requirements:
			`sudo apt install xdotool`

		Setup:
			use `xinput --list` to list input devices and note the id of your mouse
			use `watch -n0 xinput --query-state ID_OF_YOUR_MOUSE_HERE` and try the buttons on your mouse then
				note the button that you want to use as your trigger.

		Usage/Args:
	BANNER

	opts.on('--verbose', 'noisy printing') { $cfg.verbose = true }
	opts.on('--time-between SECONDS', Float, 'time between mouse clicks') { |f| $cfg.between = f }
	opts.on('--mouse MOUSE_ID', Integer, 'mouse id (get it from `xinput --list`') { |i| $cfg.mouse_id = i }
	opts.on('--trigger TRIGGER', String, 'mouse button to trigger autoclick: i.e. "button[10]=down"') { |t| $cfg.trigger = t }
end.parse!

def ex(cmd)
	puts(cmd) if $cfg.verbose
	`#{cmd}`
end

def mousedown!
	ex('xdotool mousedown 1')
end

def mouseup!
	ex('xdotool mouseup 1')
end

def trigger?
	ex("xinput --query-state #{$cfg.mouse_id}").include?($cfg.trigger)
end

def auto_click!
	sleep_time = $cfg.between / 2.0 

	while true do
		if trigger?
			mousedown!
			sleep(sleep_time)
			mouseup!
			sleep(sleep_time)
		end
	end
rescue Interrupt
	# pressing ctrl + c raises Interrupt
	# let go of the mouse to avoid odd click-drag effects for our beloved user
	mouseup!
end


puts('autoclick running...')

auto_click!

puts('autoclick shutdown.')
